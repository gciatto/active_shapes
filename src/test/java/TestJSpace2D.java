import it.unibo.gciatto.active_shapes.gui.JSpace2D;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.InputStream;

/**
 * Created by gciatto on 13/02/16.
 */
public class TestJSpace2D {
    public static void main(String... args) {
        SwingUtilities.invokeLater(() -> {
            InputStream is = TestJSpace2D.class.getResourceAsStream("img1.jpg");
            JFrame f = new JFrame("testJSpace2D");
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            JPanel cp = new JPanel();
            cp.setLayout(new BorderLayout());
            JSpace2D s = new JSpace2D();
            try {
                s.setImage(ImageIO.read(is));
            } catch (Exception e) {
                e.printStackTrace();
            }
            cp.add(s, BorderLayout.CENTER);
            f.setContentPane(cp);
            f.setSize(200, 200);
            f.setVisible(true);
        });
    }
}
