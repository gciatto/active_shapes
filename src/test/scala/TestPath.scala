import it.unibo.gciatto.math.Path
import it.unibo.gciatto.math.RealVector.tupleToRealVector
import org.junit.Test

/**
  * Created by gciatto on 13/02/16.
  */
class TestPath {

  @Test def test(): Unit = {
    val p = Path.open((0d, 0d))

    p -->(1d, 1d)

    p.insertBefore((0d, 0d))((5d, 5d))

    val p1 = Path.open((0d, 0d))
    p1 -->(2d / 2d, 2d / 2d)

    println(p)
    p1.closed = true
    println(p1)
    println(p == p1)

    val hdp = p.toHighDimensionalityVector
    println(hdp)

    val ldp = Path.openFromHDV(1)(hdp)
    println(ldp)
  }
}
