package it.unibo.gciatto.math

import org.apache.commons.math3.linear.{MatrixUtils, RealVector, RealVectorPreservingVisitor}

/**
  * Created by gciatto on 12/02/16.
  */
object RealVector {
  def one(dim: Int): RealVector =
    fill(dim)(1)

  def fill(dim: Int)(x: Double): RealVector =
    tabulate(dim)(i => x)

  def tabulate(dim: Int)(f: (Int => Double)): RealVector =
    MatrixUtils.createRealVector(Array.tabulate(dim)(f))

  def zero(dim: Int): RealVector =
    fill(dim)(0)

  implicit def tupleToRealVector(x: Tuple1[Double]): RealVector =
    apply(x._1)

  def apply(x: Double, xs: Double*) =
    MatrixUtils.createRealVector(Array(x, xs: _*))

  implicit def tupleToRealVector(x: Tuple2[Double, Double]): RealVector =
    apply(x._1, x._2)

  implicit def tupleToRealVector(x: Tuple3[Double, Double, Double]): RealVector =
    apply(x._1, x._2, x._3)

  implicit def tupleToRealVector(x: Tuple4[Double, Double, Double, Double]): RealVector =
    apply(x._1, x._2, x._3, x._4)

  implicit class RealVectorReadVisitor(f: (Int, Double) => Unit) extends RealVectorPreservingVisitor {
    override def end(): Double = -1

    override def visit(index: Int, value: Double): Unit = f(index, value)

    override def start(dimension: Int, start: Int, end: Int): Unit = {}
  }

}
