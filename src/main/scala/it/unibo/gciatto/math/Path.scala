package it.unibo.gciatto.math

import org.apache.commons.math3.exception.DimensionMismatchException
import org.apache.commons.math3.linear.{MatrixUtils, RealVector}

import scala.collection.{JavaConversions, mutable}

/**
  * Created by gciatto on 13/02/16.
  */
trait Path {
  def points: List[_ <: RealVector]

  def dimension: Int

  def size: Int

  def append(x: RealVector)

  def insertAfter(element: RealVector)(x: RealVector)

  def insertBefore(element: RealVector)(x: RealVector)

  def insertAfter(index: Int)(x: RealVector)

  def insertBefore(index: Int)(x: RealVector)

  def closed: Boolean

  def closed_=(x: Boolean)

  def apply(i: Int): RealVector

  def update(i: Int)(x: RealVector)

  def toHighDimensionalityVector: RealVector

  def -->(x: RealVector) = append(x)

  def <--(x: RealVector) = add(x)

  def add(x: RealVector) = insertBefore(0)(x)

  def -->(x: Path) = concat(x)

  def concat(other: Path) = {
    other.points.foreach(append(_))
  }

  override def toString = s"PathImpl(closed=$closed, points=(${
    points.mkString(" --> ")
  }))"

  override def equals(other: Any): Boolean = other match {
    case that: Path =>
      dimension == that.dimension &&
        closed == that.closed &&
        points == that.points
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(closed, points)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}

object Path {
  def -->(p1: RealVector, p2: RealVector): Path = {
    open(p1, p2)
  }

  def open(point: RealVector, points: RealVector*): Path =
    apply(false)(point, points: _*)

  def closed(point: RealVector, points: RealVector*): Path =
    apply(true)(point, points: _*)

  def copyOf(path: Path): Path = {
    val points = path.points
    apply(path.closed)(points.head, points.tail: _*)
  }

  def apply(close: Boolean)(point: RealVector, points: RealVector*): Path = {
    val p = new PathImpl(point.getDimension, close)
    p --> point
    points.foreach(p --> _)
    p
  }

  def deepCopyOf(path: Path): Path = {
    val points = path.points
    apply(path.closed)(points.head.copy(), points.tail.map(_.copy()): _*)
  }

  def closedFromHDV(lowDim: Int)(point: RealVector): Path = {
    fromHighDimensionalityVector(true)(lowDim)(point)
  }

  def openFromHDV(lowDim: Int)(point: RealVector): Path = {
    fromHighDimensionalityVector(false)(lowDim)(point)
  }

  def fromHighDimensionalityVector(close: Boolean)(lowDim: Int)(point: RealVector): Path = {
    val size = point.getDimension
    val p = empty(lowDim)
    val n = size / lowDim
    for (i <- 0 until n) {
      p --> RealVector.tabulate(lowDim)(j => point.getEntry(i * lowDim + j))
    }
    return p
  }

  def empty(dim: Int): Path = new PathImpl(dim, false)
}

class PathImpl(private val dim: Int, private var isClosed: Boolean) extends Path {
  private val data = new mutable.ListBuffer[RealVector]()

  override def append(realVector: RealVector): Unit = {
    data += checkSize(realVector)
  }

  @throws[DimensionMismatchException]
  protected def checkSize(realVector: RealVector): RealVector = {
    if (realVector.getDimension != dimension)
      throw new DimensionMismatchException(realVector.getDimension, dimension)

    realVector
  }

  override def dimension: Int = dim

  override def points: List[_ <: RealVector] = data.toList

  override def insertBefore(element: RealVector)(realVector: RealVector): Unit = {
    insertBefore(data indexOf element)(realVector)
  }

  override def insertBefore(index: Int)(realVector: RealVector): Unit = {
    data.insert(index, checkSize(realVector))
  }

  override def insertAfter(element: RealVector)(realVector: RealVector): Unit = {
    insertAfter(data indexOf element)(realVector)
  }

  override def insertAfter(index: Int)(realVector: RealVector): Unit = {
    insertBefore(index + 1)(realVector)
  }

  override def closed: Boolean = isClosed

  override def closed_=(x: Boolean): Unit = {
    isClosed = x
  }

  implicit def toJavaList: java.util.List[_ <: RealVector] = {
    JavaConversions.bufferAsJavaList(data)
  }

  override def apply(i: Int): RealVector = data(i)

  override def update(i: Int)(x: RealVector): Unit = data(i) = x

  override def toHighDimensionalityVector: RealVector = {
    MatrixUtils.createRealVector(
      Array.tabulate[Double](size * dimension)(i =>
        data(i / dimension).getEntry(i % dimension)
      )
    )
  }

  override def size: Int = data.size
}