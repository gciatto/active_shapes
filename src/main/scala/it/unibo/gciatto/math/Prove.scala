package it.unibo.gciatto.math

import org.apache.commons.math3.linear._

/**
  * Created by gciatto on 12/02/16.
  */
object Prove extends App {

  val m: RealMatrix = MatrixUtils.createRealMatrix(2, 2)
  val f = (i: Int, j: Int, v: Double) => j match {
    case 0 => 1d
    case 1 => if (i == 0) 1d else -1d
    //    case 2 => if (i == 0) 15d else 5d
  }
  val d = new LUDecomposition(m)
  m.walkInOptimizedOrder(f)
  val res = d.getSolver.solve(MatrixUtils.createRealVector(Array(15d, 5d)));

  implicit def toRealMatrixChangingVisitor(f: (Int, Int, Double) => Double): RealMatrixChangingVisitor =
    new RealMatrixChangingVisitor() {
      override def end(): Double = 0

      override def visit(row: Int, column: Int, value: Double): Double = f(row, column, value)

      override def start(rows: Int, columns: Int, startRow: Int, endRow: Int, startColumn: Int, endColumn: Int): Unit = {}
    }

  println(res);
}
