package it.unibo.gciatto.math

import org.apache.commons.math3.exception.DimensionMismatchException
import org.apache.commons.math3.linear.{MatrixUtils, RealMatrix, RealVector}

/**
  * Created by gciatto on 12/02/16.
  */
trait Affinity {
  def translation: RealVector

  def transformation: RealMatrix

  def combine(other: Affinity): Affinity

  def inverse: Affinity

  def dimension: Int

  override def equals(other: Any): Boolean = other match {
    case that: Affinity =>
      that.translation == translation &&
        that.transformation == transformation
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(translation, transformation)
    state.map(_.hashCode()).foldLeft(17)((a, b) => 31 * a + b)
  }

  def &(other: Affinity): Affinity =
    combine(other)
}

case class Compound(private val _transformation: RealMatrix, private val _translation: RealVector) extends Affinity {
  Seq(transformation.getColumnDimension, transformation.getRowDimension)
    .find(_ != dimension)
    .foreach(it => throw new DimensionMismatchException(it, dimension))

  private lazy val _inverse = new Compound(MatrixUtils.inverse(transformation), _translation.mapMultiply(-1d))

  override def combine(other: Affinity): Affinity =
    new Compound(transformation.multiply(other.transformation), translation.add(other.translation))

  override def transformation: RealMatrix = _transformation

  override def translation: RealVector = _translation

  override def inverse: Affinity = _inverse

  override def dimension: Int = _translation.getDimension
}

case class Translation(private val _translation: RealVector) extends Affinity {
  private lazy val _transformation = MatrixUtils.createRealIdentityMatrix(dimension)
  private lazy val _inverse = new Translation(translation.mapMultiply(-1d))

  override def combine(other: Affinity): Affinity =
    new Compound(transformation, translation.add(other.translation))

  override def transformation: RealMatrix = _transformation

  override def translation: RealVector = _translation

  override def inverse: Affinity = _inverse

  override def dimension: Int = _translation.getDimension
}

case class Rotation2D(val angle: Double) extends Affinity {
  private lazy val _translation = RealVector.zero(2)
  private lazy val _transformation = MatrixUtils.createRealMatrix(Array(
    Array(cosAngle, -sinAngle),
    Array(sinAngle, cosAngle)
  ))
  private lazy val _inverse = new Rotation2D(-angle)
  private val cosAngle = Math.cos(angle)
  private val sinAngle = Math.cos(angle)

  override def combine(other: Affinity): Affinity =
    new Compound(transformation multiply other.transformation, translation)

  override def transformation: RealMatrix = _transformation

  override def translation: RealVector = _translation

  override def inverse: Affinity = _inverse

  override def dimension: Int = 2
}

case class Scale(val scale: Double, private val scales: Double*) extends Affinity {
  //  private var temp = scales
  //  private lazy val _diagonal: Array[Double] = Array.tabulate(1 + scales.length)(i => i match {
  //    case 0 => scale
  //    case _ => {
  //      val res = temp.head
  //      temp = temp.tail
  //      res
  //    }
  //  })
  private lazy val _diagonal = Array(scale, scales: _*)
  private lazy val _translation = RealVector.zero(dimension)
  private lazy val _transformation = MatrixUtils.createRealDiagonalMatrix(_diagonal)
  private lazy val _inverse = new Scale(1 / scale, scales.map(x => 1 / x): _*)

  override def combine(other: Affinity): Affinity =
    new Compound(transformation multiply other.transformation, translation)

  override def transformation: RealMatrix = _transformation

  override def translation: RealVector = _translation

  override def inverse: Affinity = _inverse

  override def dimension: Int = _diagonal.length

  def scale(index: Int): Double =
    _diagonal(index)

}
