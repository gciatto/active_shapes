package it.unibo.gciatto.active_shapes.gui;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

/**
 * Created by gciatto on 13/02/16.
 */
abstract class JSpace2DGeom extends JSpace2DDefinition {

    private double[] atDataCache = new double[6];


    public JSpace2DGeom() {
        super();
    }

    protected Point2D pPointRelativeTo(double x, double y, Point2D relativeTo) {
        getPointCache().setLocation(x + relativeTo.getX(), y + relativeTo.getY());
        return getPointCache();
    }

    protected Point2D pDifference(Point2D after, Point2D before) {
        getPointCache().setLocation(after.getX() - before.getX(), after.getY() - before.getY());
        return getPointCache();
    }

    protected Point2D pPointRelativeToCenter(double x, double y) {
        return pPointRelativeTo(x, y, getCenter());
    }

    protected Point2D pRelativeTo(Point2D p, Point2D relativeTo) {
        getPointCache().setLocation(p.getX() + relativeTo.getX(), p.getY() + relativeTo.getY());
        return getPointCache();
    }

    protected Point2D pRelativeToCenter(Point2D p) {
        return pRelativeTo(p, getCenter());
    }

    protected Point2D pPoint(double x, double y) {
        getPointCache().setLocation(x, y);
        return getPointCache();
    }

    protected AffineTransform atIdentity() {
        getAffineTransformCache().setToIdentity();
        return getAffineTransformCache();
    }

    protected AffineTransform atScale(double factor) {
        getAffineTransformCache().scale(factor, factor);
        return getAffineTransformCache();
    }

    protected AffineTransform atZoom() {
        return atScale(getScaleFactorFromZoom());
    }

    protected AffineTransform atTranslation(Point2D transl) {
        getAffineTransformCache().translate(transl.getX(), transl.getY());
        return getAffineTransformCache();
    }

    protected AffineTransform atPosition(Point2D pos) {
        getAffineTransformCache().getMatrix(atDataCache);
        atDataCache[4] = pos.getX();
        atDataCache[5] = pos.getY();
        getAffineTransformCache().setTransform(
                atDataCache[0],
                atDataCache[1],
                atDataCache[2],
                atDataCache[3],
                atDataCache[4],
                atDataCache[5]
        );
        return getAffineTransformCache();
    }

    protected Point2D atApply(Point2D point2D) {
        getAffineTransformCache().transform(point2D, point2D);
        return point2D;
    }

    protected Point2D zoomed(Point2D p) {
        return new Point2D.Double(p.getX() * getScaleFactorFromZoom(), p.getY() * getScaleFactorFromZoom());
    }

    protected Point2D pZoomed(Point2D p) {
        getPointCache().setLocation(p.getX() * getScaleFactorFromZoom(), p.getY() * getScaleFactorFromZoom());
        return getPointCache();
    }

}
