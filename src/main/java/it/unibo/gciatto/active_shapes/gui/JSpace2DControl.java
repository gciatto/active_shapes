package it.unibo.gciatto.active_shapes.gui;

import java.awt.event.*;
import java.awt.geom.Point2D;

/**
 * Created by gciatto on 13/02/16.
 */
abstract class JSpace2DControl extends JSpace2DDrawing implements MouseListener, MouseMotionListener, MouseWheelListener, ComponentListener {

    private final Point2D mousePositionCache = new Point2D.Double(0, 0);
    private final double zoomFactor = 1d / 30d;

    public JSpace2DControl() {
        super();
        addMouseListener(this);
        addMouseMotionListener(this);
        addMouseWheelListener(this);
        addComponentListener(this);
        setFocusable(true);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    public Point2D getMousePositionCache() {
        return mousePositionCache;
    }

    public Point2D setMousePositionCache(Point2D newPoint) {
        mousePositionCache.setLocation(newPoint);
        return mousePositionCache;
    }

    public Point2D moveMousePositionCache(Point2D delta) {
        mousePositionCache.setLocation(
                mousePositionCache.getX() + delta.getX(),
                mousePositionCache.getY() + delta.getY()
        );
        return mousePositionCache;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        moveCenter(pDifference(e.getPoint(), getMousePositionCache()));
        setMousePositionCache(e.getPoint());
        repaint();
    }

    @Override
    public void componentResized(ComponentEvent e) {
        setCenter(getWidth() / 2, getHeight() / 2);
        repaint();
    }

    @Override
    public void componentHidden(ComponentEvent e) {

    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        setMousePositionCache(e.getPoint());
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        final double variation = e.getScrollAmount() * e.getPreciseWheelRotation();
        incZoom(variation * zoomFactor);
        repaint();
    }
}
