package it.unibo.gciatto.active_shapes.gui;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

/**
 * Created by gciatto on 13/02/16.
 */
public abstract class JSpace2DDrawing extends JSpace2DGeom {

    public JSpace2DDrawing() {
        super();
        setBackground(Color.GREEN);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        final Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        drawBackground(g2);
        getImage().ifPresent(i -> drawImage(g2, i));
        drawSpace(g2);
    }

    protected void drawBackground(Graphics2D g) {
        g.setBackground(getBackground());
        g.clearRect(0, 0, getWidth(), getHeight());
    }

    protected void drawImage(Graphics2D g, Image img) {
        g.setTransform(atIdentity());
        atScale(getScaleFactorFromZoom());
        g.drawImage(
                img,
                atPosition(
                        pDifference(
                                getCenter(),
                                pPoint(
                                        getScaleFactorFromZoom() * img.getWidth(null) / 2,
                                        getScaleFactorFromZoom() * img.getHeight(null) / 2
                                )
                        )
                ),
                null
        );
    }

    protected void drawSpace(Graphics2D g) {
        g.setColor(getForeground());
        drawAxes(g);
        drawPointRelativeToCenterWithZoom(g, pPoint(1, 0));
        drawPointRelativeToCenterWithZoom(g, pPoint(-1, 0));
        drawPointRelativeToCenterWithZoom(g, pPoint(0, 1));
        drawPointRelativeToCenterWithZoom(g, pPoint(0, -1));

    }

    protected void drawPointRelativeToCenterWithZoom(Graphics2D g, Point2D p) {
        drawPointRelativeToCenter(g, pZoomed(p));
    }

    protected void drawPointRelativeToCenter(Graphics2D g, Point2D p) {
        drawPointRelativeTo(g, p, getCenter());
    }

    protected void drawPointRelativeTo(Graphics2D g, Point2D p, Point2D relativeTo) {
//        atIdentity();
//        atScale(getPointSize());
//        final AffineTransform at = atTranslation(relativeTo);
//        g.setTransform(at);
//        g.translate(p.getX(), p.getY());
//        g.draw(getPointShape().toShape());
        drawPoint(g, pRelativeTo(p, relativeTo));
    }

    protected void drawPoint(Graphics2D g, Point2D p) {
        atIdentity();
        atScale(getPointSize());
        final AffineTransform at = atPosition(p);
        g.setTransform(at);
        g.draw(getPointShape().toShape());
    }


    protected void drawAxes(Graphics2D g) {
        g.setTransform(atIdentity());
        final int x = (int) Math.round(getCenter().getX());
        final int y = (int) Math.round(getCenter().getY());
        if (x >= 0 && x <= getWidth()) {
            g.drawLine(x, 0, x, getHeight());
        }
        if (y >= 0 && y <= getHeight()) {
            g.drawLine(0, y, getWidth(), y);
        }
    }
}
