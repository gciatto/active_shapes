package it.unibo.gciatto.active_shapes.gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Optional;

/**
 * Created by gciatto on 13/02/16.
 */
abstract class JSpace2DDefinition extends JPanel {

    public enum PointShape {
        ELLIPSE(new Ellipse2D.Double(-0.5, -0.5, 1, 1)),
        RECTANGLE(new Rectangle2D.Double(-0.5, -0.5, 1, 1));

        private final Shape shape;

        PointShape(Shape initShape) {
            this.shape = initShape;
        }

        public Shape toShape() {
            return shape;
        }

    }

    private final Point2D center;
    private final Point2D pointCache = new Point2D.Double(0, 0);
    private final AffineTransform atCache = new AffineTransform();
    private double zoom = 0;
    private double pointSize = 4;
    private PointShape pointShape = PointShape.RECTANGLE;
    private Optional<Image> image;

    public JSpace2DDefinition() {
        this(new Point2D.Double(0, 0));
    }

    public JSpace2DDefinition(Point2D center) {
        this.center = center;

        setBorder(new EmptyBorder(10, 10, 10, 10));
    }

    public double getPointSize() {
        return pointSize;
    }

    public void setPointSize(double pointSize) {
        this.pointSize = pointSize;
    }

    protected AffineTransform getAffineTransformCache() {
        return atCache;
    }

    public Point2D getCenter() {
        return center;
    }

    public Point2D moveCenter(Point2D delta) {
        center.setLocation(center.getX() + delta.getX(), center.getY() + delta.getY());
        return center;
    }

    public Point2D setCenter(Point2D center) {
        this.center.setLocation(center);
        return center;
    }

    public Point2D setCenter(double x, double y) {
        this.center.setLocation(x, y);
        return center;
    }

    protected Point2D getPointCache() {
        return pointCache;
    }

    public PointShape getPointShape() {
        return pointShape;
    }

    public void setPointShape(PointShape pointShape) {
        this.pointShape = pointShape;
    }

    public double getZoom() {
        return zoom;
    }

    public void setZoom(double zoom) {
        this.zoom = zoom; //Math.max(zoom, 1);
    }

    protected double getScaleFactorFromZoom() {
        return Math.pow(2, getZoom());
    }

    public void incZoom(double delta) {
        setZoom(getZoom() + delta);
    }

    public Optional<Image> getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = Optional.of(image);
    }

    public void clearImage() {
        this.image = Optional.empty();
    }
}
